<?php
/**
 * Copyright © 2016 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace MyPractice\Sales\Block\Adminhtml\Order\View;

/**
 * Order history block
 *
 * @author      Magento Core Team <core@magentocommerce.com>
 */
class Test extends \Magento\Sales\Block\Adminhtml\Order\AbstractOrder
{

    /**
     * Get Data String
     * @return string
     */
    public function getDataString()
    {
        return 'MY TEST BLOCK: ' .  $this->getParameter();
    }


}